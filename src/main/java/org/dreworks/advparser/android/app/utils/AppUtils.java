package org.dreworks.advparser.android.app.utils;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.*;

import static org.dreworks.advparser.android.app.utils.Settings.ADV_PUBLISHER_ID;

public final class AppUtils {

    public static void showAdv(final Context context, final RelativeLayout parentView) {
        final InterstitialAd interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(ADV_PUBLISHER_ID);

        final AdView adView = new AdView(context);

        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(ADV_PUBLISHER_ID);

        final RelativeLayout.LayoutParams advLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        advLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
        parentView.addView(adView, advLayoutParams);

        final AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("123")
                .build();
        adView.loadAd(adRequest);

        interstitialAd.loadAd(adRequest);

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
            }
        });
    }
}
