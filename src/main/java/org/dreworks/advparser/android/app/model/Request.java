package org.dreworks.advparser.android.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "request")
public class Request implements Serializable {

    public static final String ID = "id";
    public static final String CITY_COLUMN = "city";
    public static final String CATEGORY_COLUMN = "category";
    public static final String CAR_COLUMN = "car";
    public static final String CAR_MODEL_COLUMN = "car_model";

    @DatabaseField(columnName = ID, id = true)
    private int id;

    private Integer citySpinnerPosition;
    private Integer mainCategorySpinnerPosition;
    private Integer subCategorySpinnerPosition;
    private Integer oldAutoCarSpinnerPosition;
    private Integer oldAutoStartYearSpinnerPosition;
    private Integer oldAutoEndYearSpinnerPosition;
    private Integer oldAutoCarModelSpinnerPosition;
    private Integer oldAutoStartRunSpinnerPosition;
    private Integer oldAutoEndRunSpinnerPosition;
    private Integer oldAutoCarBodySpinnerPosition;
    private Integer oldAutoGearsSpinnerPosition;
    private Integer oldAutoStartEngineCapSpinnerPosition;
    private Integer oldAutoEndEngineCapSpinnerPosition;
    private Integer oldAutoEngineTypesSpinnerPosition;
    private Integer oldAutoDriveTypesSpinnerPosition;

    @DatabaseField(columnName = CITY_COLUMN, canBeNull = false)
    private String city;
    @DatabaseField(columnName = CATEGORY_COLUMN, canBeNull = false,
            foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 3)
    private Category category;
    @DatabaseField(columnName = CAR_COLUMN)
    private String oldAutoCar;
    private String oldAutoStartYear;
    private String oldAutoEndYear;
    @DatabaseField(columnName = CAR_MODEL_COLUMN)
    private String oldAutoCarModel;
    private String oldAutoStartRun;
    private String oldAutoEndRun;
    private String oldAutoCarBody;
    private String oldAutoGear;
    private String oldAutoStartEngineCap;
    private String oldAutoEndEngineCap;
    private String oldAutoEngineType;
    private String oldAutoDriveType;
    private String oldAutoStartPrice;
    private String oldAutoEndPrice;
    private Boolean oldAutoHasPhoto;

    private boolean subscribed;

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

    public Integer getCitySpinnerPosition() {
        return citySpinnerPosition;
    }

    public void setCitySpinnerPosition(final Integer citySpinnerPosition) {
        this.citySpinnerPosition = citySpinnerPosition;
    }

    public Integer getMainCategorySpinnerPosition() {
        return mainCategorySpinnerPosition;
    }

    public void setMainCategorySpinnerPosition(final Integer mainCategorySpinnerPosition) {
        this.mainCategorySpinnerPosition = mainCategorySpinnerPosition;
    }

    public Integer getSubCategorySpinnerPosition() {
        return subCategorySpinnerPosition;
    }

    public void setSubCategorySpinnerPosition(final Integer subCategorySpinnerPosition) {
        this.subCategorySpinnerPosition = subCategorySpinnerPosition;
    }

    public Integer getOldAutoCarSpinnerPosition() {
        return oldAutoCarSpinnerPosition;
    }

    public void setOldAutoCarSpinnerPosition(final Integer oldAutoCarSpinnerPosition) {
        this.oldAutoCarSpinnerPosition = oldAutoCarSpinnerPosition;
    }

    public String getOldAutoCar() {
        return oldAutoCar;
    }

    public void setOldAutoCar(final String oldAutoCar) {
        this.oldAutoCar = oldAutoCar;
    }

    public Integer getOldAutoStartYearSpinnerPosition() {
        return oldAutoStartYearSpinnerPosition;
    }

    public void setOldAutoStartYearSpinnerPosition(final Integer oldAutoStartYearSpinnerPosition) {
        this.oldAutoStartYearSpinnerPosition = oldAutoStartYearSpinnerPosition;
    }

    public Integer getOldAutoEndYearSpinnerPosition() {
        return oldAutoEndYearSpinnerPosition;
    }

    public void setOldAutoEndYearSpinnerPosition(final Integer oldAutoEndYearSpinnerPosition) {
        this.oldAutoEndYearSpinnerPosition = oldAutoEndYearSpinnerPosition;
    }

    public String getOldAutoStartYear() {
        return oldAutoStartYear;
    }

    public void setOldAutoStartYear(final String oldAutoStartYear) {
        this.oldAutoStartYear = oldAutoStartYear;
    }

    public String getOldAutoEndYear() {
        return oldAutoEndYear;
    }

    public void setOldAutoEndYear(final String oldAutoEndYear) {
        this.oldAutoEndYear = oldAutoEndYear;
    }

    public void setOldAutoCarModelSpinnerPosition(final Integer oldAutoCarModelSpinnerPosition) {
        this.oldAutoCarModelSpinnerPosition = oldAutoCarModelSpinnerPosition;
    }

    public Integer getOldAutoCarModelSpinnerPosition() {
        return oldAutoCarModelSpinnerPosition;
    }

    public String getOldAutoCarModel() {
        return oldAutoCarModel;
    }

    public void setOldAutoCarModel(final String oldAutoCarModel) {
        this.oldAutoCarModel = oldAutoCarModel;
    }

    public Integer getOldAutoStartRunSpinnerPosition() {
        return oldAutoStartRunSpinnerPosition;
    }

    public void setOldAutoStartRunSpinnerPosition(final Integer oldAutoStartRunSpinnerPosition) {
        this.oldAutoStartRunSpinnerPosition = oldAutoStartRunSpinnerPosition;
    }

    public Integer getOldAutoEndRunSpinnerPosition() {
        return oldAutoEndRunSpinnerPosition;
    }

    public void setOldAutoEndRunSpinnerPosition(final Integer oldAutoEndRunSpinnerPosition) {
        this.oldAutoEndRunSpinnerPosition = oldAutoEndRunSpinnerPosition;
    }

    public String getOldAutoStartRun() {
        return oldAutoStartRun;
    }

    public void setOldAutoStartRun(final String oldAutoStartRun) {
        this.oldAutoStartRun = oldAutoStartRun;
    }

    public String getOldAutoEndRun() {
        return oldAutoEndRun;
    }

    public void setOldAutoEndRun(final String oldAutoEndRun) {
        this.oldAutoEndRun = oldAutoEndRun;
    }

    public Integer getOldAutoCarBodySpinnerPosition() {
        return oldAutoCarBodySpinnerPosition;
    }

    public void setOldAutoCarBodySpinnerPosition(final Integer oldAutoCarBodySpinnerPosition) {
        this.oldAutoCarBodySpinnerPosition = oldAutoCarBodySpinnerPosition;
    }

    public String getOldAutoCarBody() {
        return oldAutoCarBody;
    }

    public void setOldAutoCarBody(final String oldAutoCarBody) {
        this.oldAutoCarBody = oldAutoCarBody;
    }

    public Integer getOldAutoGearsSpinnerPosition() {
        return oldAutoGearsSpinnerPosition;
    }

    public void setOldAutoGearsSpinnerPosition(final Integer oldAutoGearsSpinnerPosition) {
        this.oldAutoGearsSpinnerPosition = oldAutoGearsSpinnerPosition;
    }

    public String getOldAutoGear() {
        return oldAutoGear;
    }

    public void setOldAutoGear(final String oldAutoGear) {
        this.oldAutoGear = oldAutoGear;
    }

    public Integer getOldAutoStartEngineCapSpinnerPosition() {
        return oldAutoStartEngineCapSpinnerPosition;
    }

    public void setOldAutoStartEngineCapSpinnerPosition(final Integer oldAutoStartEngineCapSpinnerPosition) {
        this.oldAutoStartEngineCapSpinnerPosition = oldAutoStartEngineCapSpinnerPosition;
    }

    public Integer getOldAutoEndEngineCapSpinnerPosition() {
        return oldAutoEndEngineCapSpinnerPosition;
    }

    public void setOldAutoEndEngineCapSpinnerPosition(final Integer oldAutoEndEngineCapSpinnerPosition) {
        this.oldAutoEndEngineCapSpinnerPosition = oldAutoEndEngineCapSpinnerPosition;
    }

    public String getOldAutoStartEngineCap() {
        return oldAutoStartEngineCap;
    }

    public void setOldAutoStartEngineCap(final String oldAutoStartEngineCap) {
        this.oldAutoStartEngineCap = oldAutoStartEngineCap;
    }

    public String getOldAutoEndEngineCap() {
        return oldAutoEndEngineCap;
    }

    public void setOldAutoEndEngineCap(final String oldAutoEndEngineCap) {
        this.oldAutoEndEngineCap = oldAutoEndEngineCap;
    }

    public Integer getOldAutoEngineTypesSpinnerPosition() {
        return oldAutoEngineTypesSpinnerPosition;
    }

    public void setOldAutoEngineTypesSpinnerPosition(final Integer oldAutoEngineTypesSpinnerPosition) {
        this.oldAutoEngineTypesSpinnerPosition = oldAutoEngineTypesSpinnerPosition;
    }

    public Integer getOldAutoDriveTypesSpinnerPosition() {
        return oldAutoDriveTypesSpinnerPosition;
    }

    public void setOldAutoDriveTypesSpinnerPosition(final Integer oldAutoDriveTypesSpinnerPosition) {
        this.oldAutoDriveTypesSpinnerPosition = oldAutoDriveTypesSpinnerPosition;
    }

    public String getOldAutoEngineType() {
        return oldAutoEngineType;
    }

    public void setOldAutoEngineType(final String oldAutoEngineType) {
        this.oldAutoEngineType = oldAutoEngineType;
    }

    public String getOldAutoDriveType() {
        return oldAutoDriveType;
    }

    public void setOldAutoDriveType(final String oldAutoDriveType) {
        this.oldAutoDriveType = oldAutoDriveType;
    }

    public String getOldAutoStartPrice() {
        return oldAutoStartPrice;
    }

    public void setOldAutoStartPrice(final String oldAutoStartPrice) {
        this.oldAutoStartPrice = oldAutoStartPrice;
    }

    public String getOldAutoEndPrice() {
        return oldAutoEndPrice;
    }

    public void setOldAutoEndPrice(final String oldAutoEndPrice) {
        this.oldAutoEndPrice = oldAutoEndPrice;
    }

    public Boolean getOldAutoHasPhoto() {
        return oldAutoHasPhoto;
    }

    public void setOldAutoHasPhoto(final Boolean oldAutoHasPhoto) {
        this.oldAutoHasPhoto = oldAutoHasPhoto;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(final boolean subscribed) {
        this.subscribed = subscribed;
    }

    @Override
    public boolean equals(final Object o) {
        return o instanceof Request && ((Request) o).getId() == this.id;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }
}
