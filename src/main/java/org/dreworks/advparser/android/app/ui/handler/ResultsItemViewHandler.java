package org.dreworks.advparser.android.app.ui.handler;

import org.dreworks.advparser.android.app.model.Result;

public interface ResultsItemViewHandler {

    void viewResult(Result result);
}
