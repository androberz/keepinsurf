package org.dreworks.advparser.android.app.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import org.dreworks.advparser.android.app.model.RequestUnit;

import java.sql.SQLException;
import java.util.List;

import static org.dreworks.advparser.android.app.model.RequestUnit.UUID_COLUMN;

public class RequestUnitDaoImpl extends BaseDaoImpl<RequestUnit, String> implements RequestUnitDao {

    public RequestUnitDaoImpl(final ConnectionSource connectionSource, final Class<RequestUnit> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public RequestUnit getById(final String id) throws SQLException {
        final QueryBuilder<RequestUnit, String> queryBuilder = queryBuilder();
        queryBuilder.where().eq(UUID_COLUMN, id);
        final PreparedQuery<RequestUnit> preparedQuery = queryBuilder.prepare();
        final List<RequestUnit> resultSet = query(preparedQuery);
        if (resultSet.isEmpty()) {
            return null;
        }

        return resultSet.get(0);
    }

    @Override
    public void save(final RequestUnit requestUnit) throws SQLException {
        this.create(requestUnit);
    }

    @Override
    public int delete(final RequestUnit requestUnit) throws SQLException {
        return super.delete(requestUnit);
    }
}
