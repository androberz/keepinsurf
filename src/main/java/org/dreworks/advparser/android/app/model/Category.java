package org.dreworks.advparser.android.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "category")
public class Category implements Serializable {

    public static final String ID_COLUMN = "id";
    public static final String PARENT_NAME_COLUMN = "parent_name";
    public static final String NAME_COLUMN = "name";

    @DatabaseField(columnName = ID_COLUMN, generatedId = true)
    private int id;

    @DatabaseField(columnName = PARENT_NAME_COLUMN)
    private String parentNane;
    @DatabaseField(columnName = NAME_COLUMN)
    private String name;

    public String getParentNane() {
        return parentNane;
    }

    public void setParentNane(final String parentNane) {
        this.parentNane = parentNane;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return (parentNane != null) ? parentNane + ":" + name : name;
    }
}
