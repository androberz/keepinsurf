package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.*;

import java.util.ArrayList;
import java.util.List;

public class RequestServiceNoConnectionFixtureImpl implements RequestService {
    @Override
    public List<String> getCities() {
        return new ArrayList<String>();
    }

    @Override
    public List<Category> getCategories() {
        return new ArrayList<Category>();
    }

    @Override
    public OldAutoConfig getOldAutoConfig() {
        return new AvitoOldAutoConfig();
    }

    @Override
    public List<String> getModelsFor(final String car) {
        return new ArrayList<String>();
    }

    @Override
    public Request subscribe(final String userId, final String registrationId, final Request request) {
        return request;
    }

    @Override
    public boolean unsubscribe(final String userId, final Request request) {
        return true;
    }

    @Override
    public List<Result> getResultsFor(final String userId, final Request request) {
        return new ArrayList<>();
    }

    @Override
    public List<Request> getRequestsFor(final String userId) {
        return new ArrayList<>();
    }
}
