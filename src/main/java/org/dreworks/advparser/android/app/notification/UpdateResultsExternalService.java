package org.dreworks.advparser.android.app.notification;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.ui.activity.MainWindowActivity;
import org.json.JSONException;
import org.json.JSONObject;

import static org.dreworks.advparser.android.app.ui.activity.MainWindowActivity.PAGER_START_POSITION;

public class UpdateResultsExternalService extends IntentService {

    public static final int NOTIFICATION_ID = 1;

    public UpdateResultsExternalService() {
        super(UpdateResultsExternalService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        final Bundle extras = intent.getExtras();
        final GoogleCloudMessaging googleCloudMessaging = GoogleCloudMessaging.getInstance(this);
        final String messageType = googleCloudMessaging.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted message on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                final String updateExtra = extras.getString("update_json");
                String requestDescription;
                int updatesCount;
                try {
                    final JSONObject updateJson = new JSONObject(updateExtra);
                    requestDescription = updateJson.getString("request_description");
                    updatesCount = updateJson.getInt("updates_count");
                } catch (final JSONException e) {
                    requestDescription = "";
                    updatesCount = 0;
                }
                sendNotification("Request: " + requestDescription + "\nUpdates: " + updatesCount);
            }
        }

        UpdateResultsBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(final String msg) {
        final NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Intent intent = new Intent(this, MainWindowActivity.class);
        intent.putExtra(PAGER_START_POSITION, 0);
        final PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("BeeFirst auto")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setContentText(msg)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);

        builder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
