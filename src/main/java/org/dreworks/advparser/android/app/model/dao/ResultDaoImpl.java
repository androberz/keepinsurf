package org.dreworks.advparser.android.app.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.Result;

import java.sql.SQLException;
import java.util.List;

public class ResultDaoImpl extends BaseDaoImpl<Result, String> implements ResultDao {

    public ResultDaoImpl(final ConnectionSource connectionSource, final Class<Result> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public void save(final List<Result> results) throws SQLException {
        for (final Result result : results) {
            this.create(result);
        }
    }

    @Override
    public void remove(final Result result) throws SQLException {
        super.delete(result);
    }

    @Override
    public List<Result> getAllFor(final RequestUnit requestUnit) throws SQLException {
        return super.queryForEq(Result.REQUEST_UNIT_ID, requestUnit.getUuid());
    }

    @Override
    public void removeAllFor(final RequestUnit requestUnit) throws SQLException {
        final List<Result> results = getAllFor(requestUnit);
        super.delete(results);
    }
}
