package org.dreworks.advparser.android.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import org.dreworks.advparser.android.app.ui.fragment.AddRequestFragment;
import org.dreworks.advparser.android.app.ui.fragment.MyRequestsFragment;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    public TabsPagerAdapter(final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int index) {
        switch (index) {
            case 0: {
                return new AddRequestFragment();
            }
            case 1: {
                return new MyRequestsFragment();
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
