package org.dreworks.advparser.android.app.ui.fragment;

import android.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.ui.handler.SubscribeFinishHandler;

public class SubscribeDialogFragment extends DialogFragment {

    private SubscribeFinishHandler subscribeFinishHandler;

    public static SubscribeDialogFragment newInstance(final Request request) {
        final SubscribeDialogFragment fragment = new SubscribeDialogFragment();

        final Bundle args = new Bundle();
        args.putSerializable("request", request);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(org.dreworks.advparser.android.R.layout.fragment_subscribe_dialog, container, false);

        final Button okButton = (Button) view.findViewById(org.dreworks.advparser.android.R.id.subscribe_ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                getDialog().dismiss();
                subscribeFinishHandler.finishSubscription();
            }
        });

        final String dialogTitle = getResources().getString(org.dreworks.advparser.android.R.string.subscribe_dialog_title);
        getDialog().setTitle(dialogTitle);

        final TextView dialogText = (TextView) view.findViewById(org.dreworks.advparser.android.R.id.subscribe_dialog_text);

        final Request request = (Request) getArguments().getSerializable("request");
        final String formattedText = String.format("Auto: %s\nCar Model: %s\nStart year: %s\nEnd year: %s",
                request.getOldAutoCar(),
                request.getOldAutoCarModel(),
                request.getOldAutoStartYear(),
                request.getOldAutoEndYear());

        dialogText.setText(formattedText);

        return view;

    }

    public void setSubscribeFinishHandler(final SubscribeFinishHandler subscribeFinishHandler) {
        this.subscribeFinishHandler = subscribeFinishHandler;
    }
}
