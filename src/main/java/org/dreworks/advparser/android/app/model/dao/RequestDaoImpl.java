package org.dreworks.advparser.android.app.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import org.dreworks.advparser.android.app.model.Request;

import java.sql.SQLException;

public class RequestDaoImpl extends BaseDaoImpl<Request, Integer> implements RequestDao {

    public RequestDaoImpl(final ConnectionSource connectionSource, final Class<Request> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public Request getById(final Integer id) throws SQLException {
        return this.queryForId(id);
    }

    @Override
    public void save(final Request request) throws SQLException {
        super.create(request);
    }

    @Override
    public int refresh(final Request data) throws SQLException {
        return super.refresh(data);
    }

    @Override
    public int delete(final Request request) throws SQLException {
        return super.delete(request);
    }
}
