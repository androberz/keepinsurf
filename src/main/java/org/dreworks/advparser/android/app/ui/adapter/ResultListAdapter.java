package org.dreworks.advparser.android.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Result;

public class ResultListAdapter extends ArrayAdapter<Result> {

    private final int layoutResource;

    public ResultListAdapter(final Context context, final int layoutResource) {
        super(context, layoutResource);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View view = getWorkingView(convertView);
        final ViewHolder viewHolder = getViewHolder(view);
        final Result entry = getItem(position);

        viewHolder.date.setText(entry.getDate());
        viewHolder.description.setText(entry.getDescription());
        viewHolder.siteLink.setText(entry.getLink());

        return view;
    }

    private View getWorkingView(final View convertView) {
        final View workingView;

        if (null == convertView) {
            final LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            workingView = layoutInflater.inflate(layoutResource, null);
        } else {
            workingView = convertView;
        }

        return workingView;
    }

    private ViewHolder getViewHolder(final View workingView) {
        final Object tag = workingView.getTag();
        final ViewHolder viewHolder;

        if (null == tag || !(tag instanceof ViewHolder)) {
            viewHolder = new ViewHolder();
            viewHolder.picture = (ImageView) workingView.findViewById(R.id.result_picture);
            viewHolder.date = (TextView) workingView.findViewById(R.id.result_date);
            viewHolder.description = (TextView) workingView.findViewById(R.id.result_description);
            viewHolder.siteLink = (TextView) workingView.findViewById(R.id.result_site_link);

            workingView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) tag;
        }

        return viewHolder;
    }

    private static class ViewHolder {
        public TextView date;
        public TextView description;
        public ImageView picture;
        public TextView siteLink;
    }
}
