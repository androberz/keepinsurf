package org.dreworks.advparser.android.app.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.ui.handler.SubscribeStopHandler;

public class StopSubscribeDialogFragment extends DialogFragment {

    private SubscribeStopHandler subscribeStopHandler;

    public static StopSubscribeDialogFragment newInstance(final Request request) {
        final StopSubscribeDialogFragment fragment = new StopSubscribeDialogFragment();

        final Bundle args = new Bundle();
        args.putSerializable("request", request);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(org.dreworks.advparser.android.R.layout.fragment_subscribe_stop_dialog, container, false);

        final String dialogTitle = getResources().getString(org.dreworks.advparser.android.R.string.subscribe_stop_dialog_title);
        getDialog().setTitle(dialogTitle);

        final TextView dialogText = (TextView) view.findViewById(org.dreworks.advparser.android.R.id.subscribe_stop_dialog_text);

        final Request request = (Request) getArguments().getSerializable("request");
        final String formattedText = String.format("Auto: %s\nCar Model: %s\nStart year: %s\nEnd year: %s",
                request.getOldAutoCar(),
                request.getOldAutoCarModel(),
                request.getOldAutoStartYear(),
                request.getOldAutoEndYear());

        dialogText.setText(formattedText);

        final Button declineButton = (Button) view.findViewById(org.dreworks.advparser.android.R.id.subscribe_decline_button);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                getDialog().dismiss();
                subscribeStopHandler.stopSubscription(request);
            }
        });

        final Button cancelButton = (Button) view.findViewById(org.dreworks.advparser.android.R.id.subscribe_decline_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    public void setSubscribeStopHandler(final SubscribeStopHandler subscribeStopHandler) {
        this.subscribeStopHandler = subscribeStopHandler;
    }
}
