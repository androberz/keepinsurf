package org.dreworks.advparser.android.app.model.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.dreworks.advparser.android.app.model.Category;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.Result;
import org.dreworks.advparser.android.app.model.dao.*;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "keepinsurf.db";
    private static final int DATABASE_VERSION = 1;

    private RequestDao requestDao = null;
    private RequestUnitDao requestUnitDao = null;
    private CategoryDao categoryDao = null;
    private ResultDao resultDao = null;

    public DatabaseHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Request.class);
            TableUtils.createTable(connectionSource, Result.class);
            TableUtils.createTable(connectionSource, RequestUnit.class);
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource, final int i, final int i2) {
        try {
            TableUtils.dropTable(connectionSource, Category.class, true);
            TableUtils.dropTable(connectionSource, Request.class, true);
            TableUtils.dropTable(connectionSource, RequestUnit.class, true);
            TableUtils.dropTable(connectionSource, Result.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public RequestDao getRequestDao() throws SQLException {
        if (null == requestDao) {
            requestDao = new RequestDaoImpl(getConnectionSource(), Request.class);
        }

        return requestDao;
    }

    public RequestUnitDao getRequestUnitDao() throws SQLException {
        if (null == requestUnitDao) {
            requestUnitDao = new RequestUnitDaoImpl(getConnectionSource(), RequestUnit.class);
        }

        return requestUnitDao;
    }

    public CategoryDao getCategoryDao() throws SQLException {
        if (null == categoryDao) {
            categoryDao = new CategoryDaoImpl(getConnectionSource(), Category.class);
        }

        return categoryDao;
    }

    public ResultDao getResultDao() throws SQLException {
        if (null == resultDao) {
            resultDao = new ResultDaoImpl(getConnectionSource(), Result.class);
        }

        return resultDao;
    }

    @Override
    public void close() {
        super.close();
        requestDao = null;
        requestUnitDao = null;
        categoryDao = null;
        resultDao = null;
    }
}
