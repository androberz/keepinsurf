package org.dreworks.advparser.android.app.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class Settings {

    public static final String SITE_BASE_PATH = "https://www.avito.ru";

    //    public static final String BASE_URI = "http://192.168.1.7:8080/advparser";
    public static final String BASE_URI = "http://192.168.111.55:8080/advparser";
    public static final String CITIES_URI = "/avito/cities";
    public static final String CATEGORIES_URI = "/avito/categories";
    public static final String OLD_AUTO_CONFIG_URI = "/avito/oldautoconfig";

    public static final String PROJECT_NUMBER = "223622465317";
    public static final String ADV_PUBLISHER_ID = "ca-app-pub-6138680059880645/5089978815";

    public static int getAppVersion(final Context context) {
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (final PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
