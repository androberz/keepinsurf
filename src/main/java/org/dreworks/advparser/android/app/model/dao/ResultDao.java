package org.dreworks.advparser.android.app.model.dao;

import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.Result;

import java.sql.SQLException;
import java.util.List;

public interface ResultDao {

    void save(List<Result> results) throws SQLException;

    void remove(Result result) throws SQLException;

    List<Result> getAllFor(RequestUnit requestUnit) throws SQLException;

    void removeAllFor(RequestUnit requestUnit) throws SQLException;
}
