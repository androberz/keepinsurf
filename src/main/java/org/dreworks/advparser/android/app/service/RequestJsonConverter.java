package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.Request;
import org.json.JSONException;
import org.json.JSONObject;

public interface RequestJsonConverter {

    Request fromJson(JSONObject jsonRequest) throws JSONException;
}
