package org.dreworks.advparser.android.app.model;

import java.util.List;

public class AvitoOldAutoConfig implements OldAutoConfig {

    private List<String> cars;
    private List<String> years;
    private List<String> runs;
    private List<String> carBodies;
    private List<String> gears;
    private List<String> engineCapacities;
    private List<String> engineTypes;
    private List<String> driveTypes;

    @Override
    public List<String> getCars() {
        return cars;
    }

    @Override
    public List<String> getYears() {
        return years;
    }

    @Override
    public List<String> getRuns() {
        return runs;
    }

    @Override
    public List<String> getCarBodies() {
        return carBodies;
    }

    @Override
    public List<String> getGears() {
        return gears;
    }

    @Override
    public List<String> getEngineCapacities() {
        return engineCapacities;
    }

    @Override
    public List<String> getEngineTypes() {
        return engineTypes;
    }

    @Override
    public List<String> getDriveTypes() {
        return driveTypes;
    }

    public void setCars(final List<String> cars) {
        this.cars = cars;
    }

    public void setYears(final List<String> years) {
        this.years = years;
    }

    public void setRuns(final List<String> runs) {
        this.runs = runs;
    }

    public void setCarBodies(final List<String> carBodies) {
        this.carBodies = carBodies;
    }

    public void setGears(final List<String> gears) {
        this.gears = gears;
    }

    public void setEngineCapacities(final List<String> engineCapacities) {
        this.engineCapacities = engineCapacities;
    }

    public void setEngineTypes(final List<String> engineTypes) {
        this.engineTypes = engineTypes;
    }

    public void setDriveTypes(final List<String> driveTypes) {
        this.driveTypes = driveTypes;
    }
}
