package org.dreworks.advparser.android.app.ui.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.orm.HelperFactory;
import org.dreworks.advparser.android.app.service.RequestService;
import org.dreworks.advparser.android.app.service.RequestServiceImpl;
import org.dreworks.advparser.android.app.ui.adapter.RequestListAdapter;
import org.dreworks.advparser.android.app.ui.handler.SubscribeStopHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MyRequestsFragment extends Fragment implements SubscribeStopHandler {

    //    private final RequestService requestService = new RequestServiceFixtureImpl();
    private final RequestService requestService = new RequestServiceImpl();

    private ProgressDialog progressDialog;
    private ListView userRequests;
    private RequestListAdapter requestListAdapter;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_my_requests, container, false);
        final TextView textView = (TextView) view.findViewById(R.id.my_requests_text_view);
        textView.setText(R.string.tab1_text);

        userRequests = (ListView) view.findViewById(R.id.request_list);
        if (null != requestListAdapter) {
            userRequests.setAdapter(requestListAdapter);
        }

        userRequests.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                final Request selected = requestListAdapter.getItem(position);
                final StopSubscribeDialogFragment unsubscribeDialog = StopSubscribeDialogFragment.newInstance(selected);
                unsubscribeDialog.setSubscribeStopHandler(MyRequestsFragment.this);

                final FragmentTransaction showStopSubscribeDialogTransaction = getFragmentManager().beginTransaction();
                showStopSubscribeDialogTransaction.add(unsubscribeDialog, "Unsubscribe");
                showStopSubscribeDialogTransaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
    }

    @Override
    public void onStart() {
        super.onStart();

        final String userId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        new GetUserRequestsTask().execute(userId);
    }

    @Override
    public void setMenuVisibility(final boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            onStart();
        }
    }

    @Override
    public void stopSubscription(final Request request) {
        new StopSubscriptionTask().execute(request);
    }

    private class GetUserRequestsTask extends AsyncTask<String, Void, List<Request>> {

        private String forUserId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected List<Request> doInBackground(final String... params) {
            forUserId = params[0];
            return requestService.getRequestsFor(forUserId);
        }

        @Override
        protected void onPostExecute(final List<Request> requests) {
            super.onPostExecute(requests);
            progressDialog.dismiss();

            final List<Request> localRequests = new ArrayList<>();
            try {
                final RequestUnit requestUnit = HelperFactory.getHelper().getRequestUnitDao().getById(forUserId);
                if (null != requestUnit) {
                    final Request request = requestUnit.getRequest();
                    HelperFactory.getHelper().getRequestDao().refresh(request);
                    if (null != request) {
                        localRequests.add(request);
                    }
                }
            } catch (final SQLException e) {
                Toast.makeText(getActivity(), "Request retrieving error", Toast.LENGTH_SHORT).show();
            }

            final List<Request> mergedRequests;
            if (null != requests) {
                try {
                    mergeRequests(forUserId, requests, localRequests);
                } catch (final SQLException ignored) {
                }
                mergedRequests = requests;
            } else {
                mergedRequests = localRequests;
            }

            if (null == requestListAdapter) {
                requestListAdapter = new RequestListAdapter(getActivity(), R.layout.my_requests_list_item);
                userRequests.setAdapter(requestListAdapter);
            } else {
                requestListAdapter.clear();
            }
            for (final Request request : mergedRequests) {
                requestListAdapter.add(request);
            }
        }
    }

    private void mergeRequests(final String userId, final List<Request> fromServer, final List<Request> fromClient) throws SQLException {
        for (final Request local : fromClient) {
            if (!fromServer.contains(local)) {
                deleteRequest(userId, local);
            }
        }
        for (final Request remote : fromServer) {
            if (!fromClient.contains(remote)) {
                addRequest(userId, remote);
            }
        }
    }

    private class StopSubscriptionTask extends AsyncTask<Request, Void, Boolean> {

        private Request request;
        private String userId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(final Request... params) {
            userId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            request = params[0];
            return requestService.unsubscribe(userId, request);
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            super.onPostExecute(result);
            boolean isDbCleared = false;

            if (result) {
                try {
                    deleteRequest(userId, request);
                    isDbCleared = true;
                } catch (final SQLException e) {
                    Toast.makeText(getActivity(), "Request stop subscription error", Toast.LENGTH_SHORT).show();
                    isDbCleared = false;
                }
            }

            progressDialog.dismiss();

            if (isDbCleared) {
                requestListAdapter.remove(request);
                requestListAdapter.notifyDataSetChanged();
            }
        }
    }

    private void deleteRequest(final String userId, final Request request) throws SQLException {
        final RequestUnit requestUnit = HelperFactory.getHelper().getRequestUnitDao().getById(userId);

        HelperFactory.getHelper().getResultDao().removeAllFor(requestUnit);
        HelperFactory.getHelper().getCategoryDao().delete(request.getCategory());
        HelperFactory.getHelper().getRequestDao().delete(request);
        HelperFactory.getHelper().getRequestUnitDao().delete(requestUnit);
    }

    private void addRequest(final String userId, final Request request) throws SQLException {
        final RequestUnit requestUnit = new RequestUnit();
        requestUnit.setUuid(userId);
        requestUnit.setRequest(request);
        HelperFactory.getHelper().getCategoryDao().save(request.getCategory());
        HelperFactory.getHelper().getRequestDao().save(request);
        HelperFactory.getHelper().getRequestUnitDao().save(requestUnit);
    }
}
