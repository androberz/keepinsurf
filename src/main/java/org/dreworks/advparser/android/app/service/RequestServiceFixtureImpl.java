package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class RequestServiceFixtureImpl implements RequestService {
    @Override
    public List<String> getCities() {
        final List<String> results = new ArrayList<>();
        results.add("Нижний Новгород");
        results.add("Москва");
        results.add("Санкт Питербург");
        results.add("Рязань");
        results.add("Ростов");

        return results;
    }

    @Override
    public List<Category> getCategories() {
        final List<Category> results = new ArrayList<>();
        final Category category1 = new Category();
        category1.setParentNane("-- Транспорт --");
        category1.setName("Автомобили с пробегом");
        results.add(category1);
        final Category category2 = new Category();
        category2.setParentNane("-- Транспорт --");
        category2.setName("Новые автомобили");
        results.add(category2);
        final Category category3 = new Category();
        category3.setParentNane("-- Недвижимость --");
        category3.setName("Квартиры");
        results.add(category3);

        return results;
    }

    @Override
    public OldAutoConfig getOldAutoConfig() {
        final AvitoOldAutoConfig avitoOldAutoConfig = new AvitoOldAutoConfig();
        final List<String> carsArray =
                asList("", "AC", "Acura", "Alfa Romeo", "Alpina", "Aro", "Asia", "Aston Martin", "Audi", "BAW", "Bentley", "BMW", "Brilliance", "Bufori", "Bugatti", "Buick", "BYD", "Cadillac", "Caterham", "Changan", "ChangFeng", "Chery", "Chevrolet", "Chrysler", "Citroen", "Dacia", "Dadi", "Daewoo", "Daihatsu", "Daimler", "Derways", "Dodge", "Dong Feng", "Doninvest", "Eagle", "Ecomotors", "FAW", "Ferrari", "FIAT", "Ford", "Geely", "Geo", "GMC", "Great Wall", "Hafei", "Haima", "Honda", "Huanghai", "Hummer", "Hurtan", "Hyundai", "Infiniti", "Iran Khodro", "Isuzu", "Iveco", "JAC", "Jaguar", "Jeep", "Jinbei", "JMC", "KIA", "Lamborghini", "Lancia", "Land Rover", "Landwind", "Lexus", "LIFAN", "Lincoln", "Lotus", "Luxgen", "Mahindra", "Marussia", "Maserati", "Maybach", "Mazda", "Mercedes-Benz", "Mercury", "Metrocab", "MG", "MINI", "Mitsubishi", "Mitsuoka", "Morgan", "Morris", "Nissan", "Noble", "Oldsmobile", "Opel", "Pagani", "Peugeot", "Plymouth", "Pontiac", "Porsche", "Proton", "Renault", "Rolls-Royce", "Ronart", "Rover", "Saab", "Saleen", "Saturn", "Scion", "SEAT", "Shuanghuan", "Skoda", "SMA", "Smart", "Spyker", "SsangYong", "Subaru", "Suzuki", "Talbot", "Tata", "Tesla", "Tianma", "Tianye", "Toyota", "Trabant", "Volkswagen", "Volvo", "Vortex", "Wartburg", "Wiesmann", "Xin Kai", "ZX", "ВАЗ (LADA)", "ВИС", "ГАЗ", "ЗАЗ", "ЗИЛ", "ИЖ", "Москвич", "ТагАЗ", "УАЗ", "Другая");

        final List<String> yearsArray =
                asList("", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1985", "1980", "1970", "$lt 1960");

        final List<String> runsArray = asList("0", "50000", "200000", "500000");

        final List<String> carBodiesArray = asList("", "Седан", "Хетчбэк", "Универсал", "Внедорожник", "Кабриолет", "Кроссовер", "Купе", "Лимузин", "Минивэн", "Пикап", "Фургон", "Микроавтобус");

        final List<String> gearsArray = asList("", "Автоматическая", "Механическая");

        final List<Double> engineCapacitiesArray = asList(0.6, 3.5, 6.0);

        final List<String> engineTypesArray = asList("", "Бензиновый", "Дизельный", "Гибридный");

        final List<String> driveTypesArray = asList("", "Передний", "Задний", "Полный");

        avitoOldAutoConfig.setCars(carsArray);
        avitoOldAutoConfig.setYears(yearsArray);
        avitoOldAutoConfig.setRuns(configRuns(runsArray));
        avitoOldAutoConfig.setCarBodies(carBodiesArray);
        avitoOldAutoConfig.setGears(gearsArray);
        avitoOldAutoConfig.setEngineCapacities(configEngineCapacities(engineCapacitiesArray));
        avitoOldAutoConfig.setEngineTypes(engineTypesArray);
        avitoOldAutoConfig.setDriveTypes(driveTypesArray);

        return avitoOldAutoConfig;
    }

    private List<String> configEngineCapacities(final List<Double> origin) {
        final List<String> result = new ArrayList<>();

        for (Double i = origin.get(0); i < origin.get(1); i = i + 0.1) {
            result.add(String.format("%.1f", i));
        }
        for (Double i = origin.get(1); i <= origin.get(2); i = i + 0.5) {
            result.add(String.format("%.1f", i));
        }

        return withEmptyFirstElement(result);
    }

    @Override
    public List<String> getModelsFor(final String car) {
        return asList("", "1", "2", "3", "4", "5", "6", "7");
    }

    @Override
    public Request subscribe(final String userId, final String registrationId, final Request request) {
        request.setSubscribed(true);
        return request;
    }

    @Override
    public boolean unsubscribe(final String userId, final Request request) {
        request.setSubscribed(false);
        return true;
    }

    @Override
    public List<Result> getResultsFor(final String userId, final Request request) {
        return new ArrayList<>();
    }

    @Override
    public List<Request> getRequestsFor(final String userId) {
        return new ArrayList<>();
    }

    private List<String> configRuns(final List<String> origin) {
        final ArrayList<String> result = new ArrayList<>();

        for (Integer i = Integer.valueOf(origin.get(0)); i < Integer.valueOf(origin.get(1)); i = i + 5000) {
            result.add(i.toString());
        }
        for (Integer i = Integer.valueOf(origin.get(1)); i < Integer.valueOf(origin.get(2)); i = i + 10000) {
            result.add(i.toString());
        }
        for (Integer i = Integer.valueOf(origin.get(2)); i <= Integer.valueOf(origin.get(3)); i = i + 50000) {
            result.add(i.toString());
        }

        return withEmptyFirstElement(result);
    }

    private List<String> withEmptyFirstElement(final List<String> list) {
        final ArrayList<String> result = new ArrayList<>();
        result.add("");
        result.addAll(list);

        return result;
    }
}
