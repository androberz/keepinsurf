package org.dreworks.advparser.android.app.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.*;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.Result;
import org.dreworks.advparser.android.app.model.orm.HelperFactory;
import org.dreworks.advparser.android.app.service.RequestService;
import org.dreworks.advparser.android.app.service.RequestServiceImpl;
import org.dreworks.advparser.android.app.ui.adapter.ResultListAdapter;
import org.dreworks.advparser.android.app.ui.handler.ResultsItemViewHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.dreworks.advparser.android.app.utils.AppUtils.showAdv;

public class ResultsActivity extends FragmentActivity implements ResultsItemViewHandler {

    private final RequestService requestService = new RequestServiceImpl();

    private ProgressDialog progressDialog;
    private Request request;
    private ListView updateResults;
    private ResultListAdapter resultsAdapter;
    private boolean isUpdate;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_results);

        final TextView dialogText = (TextView) findViewById(R.id.view_results_dialog_text);

        request = (Request) getIntent().getSerializableExtra("request");
        isUpdate = getIntent().getBooleanExtra("isUpdate", false);
        final String formattedText = String.format("Auto: %s\nCar Model: %s\nStart year: %s\nEnd year: %s",
                request.getOldAutoCar(),
                request.getOldAutoCarModel(),
                request.getOldAutoStartYear(),
                request.getOldAutoEndYear());

        dialogText.setText(formattedText);

        final Button backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                finish();
            }
        });

        updateResults = (ListView) findViewById(R.id.update_results);
        if (null != resultsAdapter) {
            updateResults.setAdapter(resultsAdapter);
        }

        updateResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                final Result selected = resultsAdapter.getItem(position);
                ResultsActivity.this.viewResult(selected);
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));

        final RelativeLayout parentView = (RelativeLayout) findViewById(R.id.results_activity_layout);
        showAdv(this, parentView);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new GetResultsTask().execute(request);
    }

    @Override
    public void viewResult(final Result result) {
        final Intent webViewResultIntent = new Intent(this, ResultWebViewActivity.class);
        webViewResultIntent.putExtra("result", result);
        startActivity(webViewResultIntent);
    }

    private class GetResultsTask extends AsyncTask<Request, Void, List<Result>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected List<Result> doInBackground(final Request... params) {
            final Request request = params[0];
            final String userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

            return requestService.getResultsFor(userId, request);
        }

        @Override
        protected void onPostExecute(final List<Result> results) {
            super.onPostExecute(results);
            progressDialog.dismiss();

            final List<Result> localResults = new ArrayList<>();
            final String userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            try {
                final RequestUnit requestUnit = HelperFactory.getHelper().getRequestUnitDao().getById(userId);
                if (null != requestUnit) {
                    localResults.addAll(requestUnit.getResults());
                }
            } catch (final SQLException e) {
                Toast.makeText(ResultsActivity.this, "Results retrieving error", Toast.LENGTH_SHORT).show();
            }

            final List<Result> resultsUpdate = new ArrayList<>();
            try {
                resultsUpdate.addAll(mergeResults(userId, results, localResults));
            } catch (final SQLException ignored) {
            }

            if (null == resultsAdapter) {
                resultsAdapter = new ResultListAdapter(ResultsActivity.this, R.layout.results_dialog_list_item);
                updateResults.setAdapter(resultsAdapter);
            } else {
                resultsAdapter.clear();
            }

            for (final Result result : resultsUpdate) {
                resultsAdapter.add(result);
            }

            if (!isUpdate) {
                for (final Result localResult : localResults) {
                    resultsAdapter.add(localResult);
                }
            }
        }
    }

    private List<Result> mergeResults(final String userId, final List<Result> fromServer, final List<Result> fromClient) throws SQLException {
        final List<Result> resultsToAdd = new ArrayList<>();
        for (final Result remote : fromServer) {
            if (!fromClient.contains(remote)) {
                resultsToAdd.add(remote);
            }
        }
        addResults(userId, resultsToAdd);

        return resultsToAdd;
    }

    private void addResults(final String userId, final List<Result> results) throws SQLException {
        final RequestUnit requestUnit = HelperFactory.getHelper().getRequestUnitDao().getById(userId);
        requestUnit.addResults(results);
    }
}
