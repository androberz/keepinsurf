package org.dreworks.advparser.android.app.ui.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AvitoWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
        view.loadUrl(url);

        return true;
    }
}
