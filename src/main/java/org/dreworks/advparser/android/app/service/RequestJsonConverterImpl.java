package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.Category;
import org.dreworks.advparser.android.app.model.Request;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestJsonConverterImpl implements RequestJsonConverter {

    @Override
    public Request fromJson(final JSONObject jsonRequest) throws JSONException {
        final Request result = new Request();
        result.setId(jsonRequest.getInt("uid"));
        result.setCity(jsonRequest.getString("city"));

        final Category resultCategory = new Category();
        final JSONObject jsonCategory = jsonRequest.getJSONObject("category");
        resultCategory.setParentNane(jsonCategory.getString("parentName"));
        resultCategory.setName(jsonCategory.getString("name"));
        result.setCategory(resultCategory);

        final JSONObject jsonCriteria = jsonRequest.getJSONObject("criteria");
        result.setOldAutoCar(jsonCriteria.getString("carBrand"));
        result.setOldAutoCarModel(jsonCriteria.getString("model"));
        result.setOldAutoStartYear(jsonCriteria.getString("startYear"));
        result.setOldAutoEndYear(jsonCriteria.getString("endYear"));
        result.setOldAutoStartEngineCap(jsonCriteria.getString("startEngineCapacity"));
        result.setOldAutoEndEngineCap(jsonCriteria.getString("endEngineCapacity"));
        result.setOldAutoStartRun(jsonCriteria.getString("startRun"));
        result.setOldAutoEndRun(jsonCriteria.getString("endRun"));
        result.setOldAutoStartPrice(jsonCriteria.getString("startPrice"));
        result.setOldAutoEndPrice(jsonCriteria.getString("endPrice"));
        result.setOldAutoEngineType(jsonCriteria.getString("engineType"));
        result.setOldAutoHasPhoto(jsonCriteria.getBoolean("hasPhoto"));
        result.setOldAutoDriveType(jsonCriteria.getString("driveType"));
        result.setOldAutoCarBody(jsonCriteria.getString("carBody"));
        result.setOldAutoGear(jsonCriteria.getString("gearType"));

        return result;
    }
}
