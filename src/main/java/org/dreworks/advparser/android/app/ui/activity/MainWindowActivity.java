package org.dreworks.advparser.android.app.ui.activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.RelativeLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.ui.adapter.TabsPagerAdapter;
import org.dreworks.advparser.android.app.ui.handler.RequestListEntryViewResultsHandler;
import org.dreworks.advparser.android.app.ui.listener.MainWindowTabListener;
import org.dreworks.advparser.android.app.utils.Settings;

import java.io.IOException;

import static android.app.ActionBar.NAVIGATION_MODE_TABS;
import static org.dreworks.advparser.android.app.utils.AppUtils.showAdv;

public class MainWindowActivity extends FragmentActivity implements RequestListEntryViewResultsHandler {

    public static final String PAGER_START_POSITION = "pagerStartPosition";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final String SENDER_ID = Settings.PROJECT_NUMBER;

    private GoogleCloudMessaging googleCloudMessaging;
    private String registrationId;
    private Context context;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final int pagerStartPosition = getIntent().getIntExtra(PAGER_START_POSITION, 0);

        setContentView(R.layout.activity_main_window);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ActionBar actionBar = getActionBar();
        final TabsPagerAdapter pagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(pagerAdapter);
        final ActionBar.Tab tab1;
        final ActionBar.Tab tab2;
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
            actionBar.setNavigationMode(NAVIGATION_MODE_TABS);

            tab1 = actionBar.newTab().setText(getString(R.string.tab1_text));
            tab2 = actionBar.newTab().setText(getString(R.string.tab2_text));

            final MainWindowTabListener listener = new MainWindowTabListener(viewPager);
            tab1.setTabListener(listener);
            tab2.setTabListener(listener);

            actionBar.addTab(tab1);
            actionBar.addTab(tab2);

            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(final int i, final float v, final int i2) {
                }

                @Override
                public void onPageSelected(final int position) {
                    actionBar.setSelectedNavigationItem(position);
                }

                @Override
                public void onPageScrollStateChanged(final int i) {
                }
            });
        }

        viewPager.setCurrentItem(pagerStartPosition);

        if (checkPlayServices()) {
            googleCloudMessaging = GoogleCloudMessaging.getInstance(this);
            context = getApplicationContext();
            registrationId = getRegistrationId(context);

            if (registrationId.isEmpty()) {
                registerInBackground();
            }
        }

        final RelativeLayout parentView = (RelativeLayout) findViewById(R.id.main_activity_layout);
        showAdv(this, parentView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPlayServices();
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main_window);
    }

    @Override
    public void viewResults(final Request request, final boolean isUpdate) {
        final Intent viewResultsIntent = new Intent(this, ResultsActivity.class);
        viewResultsIntent.putExtra("request", request);
        viewResultsIntent.putExtra("isUpdate", isUpdate);
        startActivity(viewResultsIntent);
    }

    public String getRegistrationId() {
        return registrationId;
    }

    private boolean checkPlayServices() {
        final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }

        return true;
    }

    private String getRegistrationId(final Context context) {
        final SharedPreferences gcmPreferences = getGCMPreferences();
        final String registrationId = gcmPreferences.getString(PROPERTY_REG_ID, "");

        if (registrationId.isEmpty()) {
            return "";
        }

        final int registeredVersion = gcmPreferences.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        final int currentVersion = Settings.getAppVersion(context);

        if (registeredVersion != currentVersion) {
            return "";
        }

        return registrationId;
    }

    private SharedPreferences getGCMPreferences() {
        return getSharedPreferences(MainWindowActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(final Void... params) {
                String msg;

                try {
                    if (null == googleCloudMessaging) {
                        googleCloudMessaging = GoogleCloudMessaging.getInstance(context);
                    }

                    registrationId = googleCloudMessaging.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + registrationId;

                    storeRegistrationId(context, registrationId);
                } catch (final IOException e) {
                    msg = "Error :" + e.getMessage();
                }

                return msg;
            }

            @Override
            protected void onPostExecute(final String msg) {
                super.onPostExecute(msg);
            }
        }.execute();
    }

    private void storeRegistrationId(final Context context, final String registrationId) {
        final SharedPreferences prefs = getGCMPreferences();
        final int appVersion = Settings.getAppVersion(context);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, registrationId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }
}
