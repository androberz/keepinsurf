package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.Result;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultJsonConverterImpl implements ResultJsonConverter {

    @Override
    public Result fromJson(final JSONObject jsonResult) throws JSONException {
        final Result result = new Result();

        result.setPhotoLink(jsonResult.getString("photoLink"));
        result.setSiteId(jsonResult.getString("id"));
        result.setDate(jsonResult.getString("date"));
        result.setDescription(jsonResult.getString("description"));
        result.setLink(jsonResult.getString("link"));

        return result;
    }
}
