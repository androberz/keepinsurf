package org.dreworks.advparser.android.app.ui.fragment;

import android.accounts.NetworkErrorException;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Category;
import org.dreworks.advparser.android.app.model.OldAutoConfig;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.model.RequestUnit;
import org.dreworks.advparser.android.app.model.orm.HelperFactory;
import org.dreworks.advparser.android.app.service.CategoryMapper;
import org.dreworks.advparser.android.app.service.RequestService;
import org.dreworks.advparser.android.app.service.RequestServiceImpl;
import org.dreworks.advparser.android.app.ui.activity.MainWindowActivity;
import org.dreworks.advparser.android.app.ui.handler.SubscribeFinishHandler;

import java.sql.SQLException;
import java.util.*;

import static android.R.layout.simple_spinner_dropdown_item;
import static android.provider.Settings.Secure;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

public class AddRequestFragment extends Fragment implements SubscribeFinishHandler {

    //    private final RequestService requestService = new RequestServiceFixtureImpl();
    private final RequestService requestService = new RequestServiceImpl();
    private final Request request = new Request();
    private ProgressDialog progressDialog;
    private Spinner citySpinner;
    private Button nextButton;
    private ArrayAdapter<String> cityAdapter;
    private Spinner mainCategorySpinner;
    private ArrayAdapter<String> mainCategoryAdapter;
    private Integer step = 0;
    private Button retryButton;
    private View.OnClickListener currentRetryListener;
    private Button prevButton;
    private Spinner subCategorySpinner;
    private Map<String, List<String>> categoryMap = new HashMap<>();
    private AdapterView.OnItemSelectedListener mainCategorySelectListener;
    private LinearLayout configContainer;
    private Spinner carsConfigSpinner;
    private Spinner startYearsConfigSpinner;
    private Spinner endYearsConfigSpinner;
    private ArrayAdapter<String> carsConfigAdapter;
    private ArrayAdapter<String> yearsConfigAdapter;
    private LinearLayout carModelsContainer;
    private Spinner carModelsConfigSpinner;
    private ArrayAdapter<String> carModelsConfigAdapter;
    private AdapterView.OnItemSelectedListener carsConfigSpinnerListener;
    private Spinner startRunsConfigSpinner;
    private Spinner endRunsConfigSpinner;
    private ArrayAdapter<String> runsConfigAdapter;
    private Spinner carBodyConfigSpinner;
    private ArrayAdapter<String> carBodyConfigAdapter;
    private Spinner gearsConfigSpinner;
    private ArrayAdapter<String> gearsConfigAdapter;
    private Spinner startEngineCapSpinner;
    private Spinner endEngineCapSpinner;
    private ArrayAdapter<String> engineCapsConfigAdapter;
    private Spinner engineTypeConfigSpinner;
    private Spinner driveTypeConfigSpinner;
    private ArrayAdapter<String> engineTypesConfigAdapter;
    private ArrayAdapter<String> driveTypesConfigAdapter;
    private ViewGroup addRequestContainer;
    private View oldAutoConfigView;
    private EditText startPriceInput;
    private EditText endPriceInput;
    private CheckBox hasPhotoCheckbox;

    private final CategoryMapper categoryMapper = new CategoryMapper();
    private String registrationId;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_request, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addRequestContainer = (ViewGroup) view.findViewById(R.id.add_request_container);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        nextButton = (Button) view.findViewById(R.id.next_button);
        prevButton = (Button) view.findViewById(R.id.prev_button);
        retryButton = (Button) view.findViewById(R.id.retry_button);

        citySpinner = (Spinner) view.findViewById(R.id.city_spinner);
        mainCategorySpinner = (Spinner) view.findViewById(R.id.main_category_spinner);
        subCategorySpinner = (Spinner) view.findViewById(R.id.sub_category_spinner);

        mainCategorySelectListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                String key = null;
                final Iterator<String> iterator = categoryMap.keySet().iterator();
                for (int i = 0; i <= position; i++) {
                    if (iterator.hasNext()) {
                        key = iterator.next();
                    }
                }
                final ArrayAdapter<String> subCategoryAdapter = new ArrayAdapter<>(
                        getActivity(),
                        simple_spinner_dropdown_item,
                        categoryMapper.getFriendlyCategories(categoryMap.get(key)));
                subCategorySpinner.setAdapter(subCategoryAdapter);
                final Integer subCategorySpinnerPosition = request.getSubCategorySpinnerPosition();
                if (null != subCategorySpinnerPosition) {
                    subCategorySpinner.setSelection(subCategorySpinnerPosition);
                }
                subCategorySpinner.setVisibility(VISIBLE);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
            }
        };

        carsConfigSpinnerListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                final String car = carsConfigSpinner.getItemAtPosition(position).toString();
                if (!((Integer) position).equals(request.getOldAutoCarSpinnerPosition())) {
                    if (!"".equals(car)) {
                        new GetCarModelsConfigTask().execute(car);
                    } else {
                        carModelsContainer.setVisibility(GONE);
                    }
                } else {
                    if ("".equals(car)) {
                        carModelsContainer.setVisibility(GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();

        registrationId = ((MainWindowActivity) getActivity()).getRegistrationId();

        if (step == -1) {
            retryButton.setVisibility(VISIBLE);
            retryButton.setOnClickListener(currentRetryListener);
        } else if (step == 0) {
            new GetCitiesTask().execute();
        } else if (step == 1) {
            citySpinner.setVisibility(VISIBLE);
            citySpinner.setAdapter(cityAdapter);

            final Integer citySpinnerPosition = request.getCitySpinnerPosition();
            if (null != citySpinnerPosition) {
                citySpinner.setSelection(citySpinnerPosition);
            }
            nextButton.setVisibility(VISIBLE);
        } else if (step == 2) {
            mainCategorySpinner.setVisibility(VISIBLE);
            mainCategorySpinner.setAdapter(mainCategoryAdapter);

            if (null != mainCategorySelectListener) {
                mainCategorySpinner.setOnItemSelectedListener(mainCategorySelectListener);
            }
            final Integer mainCategorySpinnerPosition = request.getMainCategorySpinnerPosition();
            if (null != mainCategorySpinnerPosition) {
                mainCategorySpinner.setSelection(mainCategorySpinnerPosition);
            }

            nextButton.setVisibility(VISIBLE);
            prevButton.setVisibility(VISIBLE);
        } else if (step == 3) {
            addRequestContainer.addView(oldAutoConfigView);
            carsConfigSpinner.setAdapter(carsConfigAdapter);
            if (null != carsConfigSpinnerListener) {
                carsConfigSpinner.setOnItemSelectedListener(carsConfigSpinnerListener);
            }
            final Integer oldAutoCarSpinnerPosition = request.getOldAutoCarSpinnerPosition();
            if (null != oldAutoCarSpinnerPosition) {
                carsConfigSpinner.setSelection(oldAutoCarSpinnerPosition);
            }

            startYearsConfigSpinner.setAdapter(yearsConfigAdapter);
            endYearsConfigSpinner.setAdapter(yearsConfigAdapter);
            final Integer oldAutoStartYearSpinnerPosition = request.getOldAutoStartYearSpinnerPosition();
            if (null != oldAutoStartYearSpinnerPosition) {
                startYearsConfigSpinner.setSelection(oldAutoStartYearSpinnerPosition);
            }

            final Integer oldAutoEndYearSpinnerPosition = request.getOldAutoEndYearSpinnerPosition();
            if (null != oldAutoEndYearSpinnerPosition) {
                endYearsConfigSpinner.setSelection(oldAutoEndYearSpinnerPosition);
            }

            carModelsConfigSpinner.setAdapter(carModelsConfigAdapter);
            final Integer oldAutoCarModelSpinnerPosition = request.getOldAutoCarModelSpinnerPosition();
            if (null != oldAutoCarModelSpinnerPosition) {
                carModelsConfigSpinner.setSelection(oldAutoCarModelSpinnerPosition);
            }

            startRunsConfigSpinner.setAdapter(runsConfigAdapter);
            endRunsConfigSpinner.setAdapter(runsConfigAdapter);
            final Integer oldAutoStartRunSpinnerPosition = request.getOldAutoStartRunSpinnerPosition();
            if (null != oldAutoStartRunSpinnerPosition) {
                startRunsConfigSpinner.setSelection(oldAutoStartRunSpinnerPosition);
            }

            final Integer oldAutoEndRunSpinnerPosition = request.getOldAutoEndRunSpinnerPosition();
            if (null != oldAutoEndRunSpinnerPosition) {
                endRunsConfigSpinner.setSelection(oldAutoEndRunSpinnerPosition);
            }

            carBodyConfigSpinner.setAdapter(carBodyConfigAdapter);
            final Integer oldAutoCarBodySpinnerPosition = request.getOldAutoCarBodySpinnerPosition();
            if (null != oldAutoCarBodySpinnerPosition) {
                carBodyConfigSpinner.setSelection(oldAutoCarBodySpinnerPosition);
            }

            gearsConfigSpinner.setAdapter(gearsConfigAdapter);
            final Integer oldAutoGearsSpinnerPosition = request.getOldAutoGearsSpinnerPosition();
            if (null != oldAutoGearsSpinnerPosition) {
                gearsConfigSpinner.setSelection(oldAutoGearsSpinnerPosition);
            }

            startEngineCapSpinner.setAdapter(engineCapsConfigAdapter);
            endEngineCapSpinner.setAdapter(engineCapsConfigAdapter);
            final Integer oldAutoStartEngineCapSpinnerPosition = request.getOldAutoStartEngineCapSpinnerPosition();
            if (null != oldAutoStartEngineCapSpinnerPosition) {
                startEngineCapSpinner.setSelection(oldAutoStartEngineCapSpinnerPosition);
            }

            final Integer oldAutoEndEngineCapSpinnerPosition = request.getOldAutoEndEngineCapSpinnerPosition();
            if (null != oldAutoEndEngineCapSpinnerPosition) {
                endEngineCapSpinner.setSelection(oldAutoEndEngineCapSpinnerPosition);
            }

            engineTypeConfigSpinner.setAdapter(engineTypesConfigAdapter);
            final Integer oldAutoEngineTypesSpinnerPosition = request.getOldAutoEngineTypesSpinnerPosition();
            if (null != oldAutoEngineTypesSpinnerPosition) {
                engineTypeConfigSpinner.setSelection(oldAutoEngineTypesSpinnerPosition);
            }

            driveTypeConfigSpinner.setAdapter(driveTypesConfigAdapter);
            final Integer oldAutoDriveTypesSpinnerPosition = request.getOldAutoDriveTypesSpinnerPosition();
            if (null != oldAutoDriveTypesSpinnerPosition) {
                driveTypeConfigSpinner.setSelection(oldAutoDriveTypesSpinnerPosition);
            }

            final String oldAutoStartPrice = request.getOldAutoStartPrice();
            if (null != oldAutoStartPrice) {
                startPriceInput.setText(oldAutoStartPrice);
            }

            final String oldAutoEndPrice = request.getOldAutoEndPrice();
            if (null != oldAutoEndPrice) {
                endPriceInput.setText(oldAutoEndPrice);
            }

            final Boolean oldAutoHasPhoto = request.getOldAutoHasPhoto();
            if (null != oldAutoHasPhoto) {
                hasPhotoCheckbox.setChecked(oldAutoHasPhoto);
            }

            configContainer.setVisibility(VISIBLE);
            carModelsContainer.setVisibility(VISIBLE);
            nextButton.setVisibility(VISIBLE);
            prevButton.setVisibility(VISIBLE);
        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (step == 1) {
                    request.setCitySpinnerPosition(citySpinner.getSelectedItemPosition());
                    request.setCity(citySpinner.getSelectedItem().toString());
                    citySpinner.setVisibility(GONE);
                    if (null == mainCategoryAdapter) {
                        new GetCategoriesTask().execute();
                    } else {
                        step = 2;
                        onStart();
                    }
                } else if (step == 2) {
                    final Category category = new Category();
                    category.setParentNane(categoryMapper.getDbName(mainCategorySpinner.getSelectedItem().toString()));
                    category.setName(categoryMapper.getDbName(subCategorySpinner.getSelectedItem().toString()));
                    request.setCategory(category);
                    request.setMainCategorySpinnerPosition(mainCategorySpinner.getSelectedItemPosition());
                    request.setSubCategorySpinnerPosition(subCategorySpinner.getSelectedItemPosition());

                    if (category.getName().equals("oldAuto")) {
                        if (null == oldAutoConfigView) {
                            initializeOldAutoConfigView();
                        }
                        mainCategorySpinner.setVisibility(GONE);
                        subCategorySpinner.setVisibility(GONE);

                        if (null == carsConfigAdapter) {
                            new GetOldAutoConfigTask().execute();
                        } else {
                            step = 3;
                            onStart();
                        }
                    }
                } else if (step == 3) {
                    request.setOldAutoCar(carsConfigSpinner.getSelectedItem().toString());
                    if (carModelsConfigAdapter != null) {
                        request.setOldAutoCarModel(carModelsConfigSpinner.getSelectedItem().toString());
                    }
                    request.setOldAutoStartYear(startYearsConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoEndYear(endYearsConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoStartRun(startRunsConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoEndRun(endRunsConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoCarBody(carBodyConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoGear(gearsConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoStartEngineCap(startEngineCapSpinner.getSelectedItem().toString());
                    request.setOldAutoEndEngineCap(endEngineCapSpinner.getSelectedItem().toString());
                    request.setOldAutoEngineType(engineTypeConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoDriveType(driveTypeConfigSpinner.getSelectedItem().toString());
                    request.setOldAutoStartPrice(startPriceInput.getText().toString());
                    request.setOldAutoEndPrice(endPriceInput.getText().toString());
                    request.setOldAutoHasPhoto(hasPhotoCheckbox.isChecked());

                    new SubscribeRequestTask().execute(request);
                }
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (step == 2) {
                    step = 1;
                    request.setMainCategorySpinnerPosition(mainCategorySpinner.getSelectedItemPosition());
                    request.setSubCategorySpinnerPosition(subCategorySpinner.getSelectedItemPosition());

                    mainCategorySpinner.setVisibility(GONE);
                    subCategorySpinner.setVisibility(GONE);
                    prevButton.setVisibility(GONE);
                    onStart();
                } else if (step == 3) {
                    step = 2;
                    request.setOldAutoCarSpinnerPosition(carsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoCarModelSpinnerPosition(carModelsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoStartYearSpinnerPosition(startYearsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoEndYearSpinnerPosition(endYearsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoStartRunSpinnerPosition(startRunsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoEndRunSpinnerPosition(endRunsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoCarBodySpinnerPosition(carBodyConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoGearsSpinnerPosition(gearsConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoStartEngineCapSpinnerPosition(startEngineCapSpinner.getSelectedItemPosition());
                    request.setOldAutoEndEngineCapSpinnerPosition(endEngineCapSpinner.getSelectedItemPosition());
                    request.setOldAutoEngineTypesSpinnerPosition(engineTypeConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoDriveTypesSpinnerPosition(driveTypeConfigSpinner.getSelectedItemPosition());
                    request.setOldAutoStartPrice(startPriceInput.getText().toString());
                    request.setOldAutoEndPrice(endPriceInput.getText().toString());
                    request.setOldAutoHasPhoto(hasPhotoCheckbox.isChecked());

                    addRequestContainer.removeView(oldAutoConfigView);

                    configContainer.setVisibility(GONE);
                    onStart();
                }
            }
        });
    }

    @Override
    public void onStop() {
        if (step == 1) {
            request.setCitySpinnerPosition(citySpinner.getSelectedItemPosition());
        } else if (step == 2) {
            request.setMainCategorySpinnerPosition(mainCategorySpinner.getSelectedItemPosition());
            request.setSubCategorySpinnerPosition(subCategorySpinner.getSelectedItemPosition());
        } else if (step == 3) {
            request.setOldAutoCarSpinnerPosition(carsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoCarModelSpinnerPosition(carModelsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoStartYearSpinnerPosition(startYearsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoEndYearSpinnerPosition(endYearsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoStartRunSpinnerPosition(startRunsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoEndRunSpinnerPosition(endRunsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoCarBodySpinnerPosition(carBodyConfigSpinner.getSelectedItemPosition());
            request.setOldAutoGearsSpinnerPosition(gearsConfigSpinner.getSelectedItemPosition());
            request.setOldAutoStartEngineCapSpinnerPosition(startEngineCapSpinner.getSelectedItemPosition());
            request.setOldAutoEndEngineCapSpinnerPosition(endEngineCapSpinner.getSelectedItemPosition());
            request.setOldAutoEngineTypesSpinnerPosition(engineTypeConfigSpinner.getSelectedItemPosition());
            request.setOldAutoDriveTypesSpinnerPosition(driveTypeConfigSpinner.getSelectedItemPosition());
            request.setOldAutoStartPrice(startPriceInput.getText().toString());
            request.setOldAutoEndPrice(endPriceInput.getText().toString());
            request.setOldAutoHasPhoto(hasPhotoCheckbox.isChecked());

            addRequestContainer.removeView(oldAutoConfigView);
        }

        super.onStop();
    }

    @SuppressWarnings("unchecked")
    private <V extends View> V inflate(final int resource, final ViewGroup root, final boolean attachToRoot) {
        return (V) LayoutInflater.from(getActivity()).inflate(resource, root, attachToRoot);
    }

    private void initializeOldAutoConfigView() {
        oldAutoConfigView = inflate(R.layout.old_auto_config, addRequestContainer, false);
        addRequestContainer.addView(oldAutoConfigView);

        configContainer = (LinearLayout) getView().findViewById(R.id.config_container);
        carsConfigSpinner = (Spinner) getView().findViewById(R.id.cars_selector);
        startYearsConfigSpinner = (Spinner) getView().findViewById(R.id.start_years_selector);
        endYearsConfigSpinner = (Spinner) getView().findViewById(R.id.end_years_selector);
        startRunsConfigSpinner = (Spinner) getView().findViewById(R.id.start_runs_selector);
        endRunsConfigSpinner = (Spinner) getView().findViewById(R.id.end_runs_selector);
        carBodyConfigSpinner = (Spinner) getView().findViewById(R.id.car_body_selector);
        gearsConfigSpinner = (Spinner) getView().findViewById(R.id.gears_selector);
        startEngineCapSpinner = (Spinner) getView().findViewById(R.id.start_engine_cap_selector);
        endEngineCapSpinner = (Spinner) getView().findViewById(R.id.end_engine_cap_selector);
        engineTypeConfigSpinner = (Spinner) getView().findViewById(R.id.engine_type_selector);
        driveTypeConfigSpinner = (Spinner) getView().findViewById(R.id.drive_type_selector);
        startPriceInput = (EditText) getView().findViewById(R.id.start_price_input);
        endPriceInput = (EditText) getView().findViewById(R.id.end_price_input);
        hasPhotoCheckbox = (CheckBox) getView().findViewById(R.id.has_photo_checkbox);

        carModelsContainer = (LinearLayout) getView().findViewById(R.id.car_models_container);
        carModelsConfigSpinner = (Spinner) getView().findViewById(R.id.car_models_selector);
    }

    @Override
    public void finishSubscription() {
        step = 1;
        addRequestContainer.removeView(oldAutoConfigView);
        configContainer.setVisibility(GONE);
        prevButton.setVisibility(GONE);
        onStart();
    }

    private class GetCitiesTask extends AsyncTask<Void, Void, List<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nextButton.setVisibility(GONE);
            progressDialog.show();
        }

        @Override
        protected List<String> doInBackground(final Void... voids) {
            return requestService.getCities();
        }

        @Override
        protected void onPostExecute(final List<String> result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            step = 1;

            if (!result.isEmpty()) {
                cityAdapter = new ArrayAdapter<>(
                        getActivity(),
                        android.R.layout.simple_spinner_dropdown_item,
                        result);

                citySpinner.setAdapter(cityAdapter);
                citySpinner.setVisibility(VISIBLE);

                nextButton.setVisibility(VISIBLE);
                retryButton.setVisibility(GONE);
            } else {
                step = -1;
                makeText(getActivity(), R.string.server_connection_error, LENGTH_SHORT).show();
                retryButton.setVisibility(VISIBLE);
                currentRetryListener = new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        new GetCitiesTask().execute();
                    }
                };
                retryButton.setOnClickListener(currentRetryListener);
            }
        }
    }

    private class GetCategoriesTask extends AsyncTask<Void, Void, List<Category>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nextButton.setVisibility(GONE);
            progressDialog.show();
        }

        @Override
        protected List<Category> doInBackground(final Void... params) {
            return requestService.getCategories();
        }

        @Override
        protected void onPostExecute(final List<Category> categories) {
            super.onPostExecute(categories);
            progressDialog.dismiss();
            step = 2;

            if (!categories.isEmpty()) {
                categoryMap = new HashMap<>();
                for (final Category category : categories) {
                    if (categoryMap.containsKey(category.getParentNane())) {
                        final List<String> subCategories = categoryMap.get(category.getParentNane());
                        subCategories.add(category.getName());
                    } else {
                        final List<String> subCategories = new ArrayList<>();
                        subCategories.add(category.getName());
                        categoryMap.put(category.getParentNane(), subCategories);
                    }
                }

                final List<String> mainCategories = new ArrayList<>();
                mainCategories.addAll(categoryMap.keySet());
                mainCategoryAdapter = new ArrayAdapter<>(
                        getActivity(),
                        simple_spinner_dropdown_item,
                        categoryMapper.getFriendlyCategories(mainCategories));

                mainCategorySpinner.setAdapter(mainCategoryAdapter);
                mainCategorySpinner.setOnItemSelectedListener(mainCategorySelectListener);

                mainCategorySpinner.setVisibility(VISIBLE);
                nextButton.setVisibility(VISIBLE);
                prevButton.setVisibility(VISIBLE);
                retryButton.setVisibility(GONE);
            } else {
                step = -1;
                makeText(getActivity(), R.string.server_connection_error, LENGTH_SHORT).show();
                retryButton.setVisibility(VISIBLE);
                currentRetryListener = new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        new GetCategoriesTask().execute();
                    }
                };
                retryButton.setOnClickListener(currentRetryListener);
            }
        }
    }

    private class GetOldAutoConfigTask extends AsyncTask<Void, Void, OldAutoConfig> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nextButton.setVisibility(GONE);
            progressDialog.show();
        }

        @Override
        protected OldAutoConfig doInBackground(final Void... params) {
            return requestService.getOldAutoConfig();
        }

        @Override
        protected void onPostExecute(final OldAutoConfig config) {
            super.onPostExecute(config);
            progressDialog.dismiss();
            step = 3;

            try {
                final List<String> cars = config.getCars();
                if (cars != null && !cars.isEmpty()) {
                    carsConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            cars);

                    carsConfigSpinner.setAdapter(carsConfigAdapter);
                    carsConfigSpinner.setOnItemSelectedListener(carsConfigSpinnerListener);

                    configContainer.setVisibility(VISIBLE);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> years = config.getYears();
                if (years.size() != 1) {
                    yearsConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            years
                    );

                    startYearsConfigSpinner.setAdapter(yearsConfigAdapter);
                    endYearsConfigSpinner.setAdapter(yearsConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> runs = config.getRuns();
                if (runs.size() != 1) {
                    runsConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            runs
                    );

                    startRunsConfigSpinner.setAdapter(runsConfigAdapter);
                    endRunsConfigSpinner.setAdapter(runsConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> carBodies = config.getCarBodies();
                if (carBodies.size() != 1) {
                    carBodyConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            carBodies
                    );

                    carBodyConfigSpinner.setAdapter(carBodyConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> gears = config.getGears();
                if (gears.size() != 1) {
                    gearsConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            gears
                    );

                    gearsConfigSpinner.setAdapter(gearsConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> engineCaps = config.getEngineCapacities();
                if (engineCaps.size() != 1) {
                    engineCapsConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            engineCaps
                    );

                    startEngineCapSpinner.setAdapter(engineCapsConfigAdapter);
                    endEngineCapSpinner.setAdapter(engineCapsConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> engineTypes = config.getEngineTypes();
                if (engineTypes.size() != 1) {
                    engineTypesConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            engineTypes
                    );

                    engineTypeConfigSpinner.setAdapter(engineTypesConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                final List<String> driveTypes = config.getDriveTypes();
                if (driveTypes.size() != 1) {
                    driveTypesConfigAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            driveTypes
                    );

                    driveTypeConfigSpinner.setAdapter(driveTypesConfigAdapter);
                } else {
                    throw new NetworkErrorException();
                }

                nextButton.setVisibility(VISIBLE);
                prevButton.setVisibility(VISIBLE);

            } catch (final NetworkErrorException e) {
                step = -1;
                makeText(getActivity(), R.string.server_connection_error, LENGTH_SHORT).show();
                retryButton.setVisibility(VISIBLE);
                currentRetryListener = new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        new GetOldAutoConfigTask().execute();
                    }
                };
                retryButton.setOnClickListener(currentRetryListener);
            }
        }
    }

    private class GetCarModelsConfigTask extends AsyncTask<String, Void, List<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected List<String> doInBackground(final String... params) {
            return requestService.getModelsFor(params[0]);
        }

        @Override
        protected void onPostExecute(final List<String> models) {
            super.onPostExecute(models);
            progressDialog.dismiss();

            carModelsConfigAdapter = new ArrayAdapter<>(
                    getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    models
            );

            carModelsConfigSpinner.setAdapter(carModelsConfigAdapter);
            carModelsContainer.setVisibility(VISIBLE);
        }
    }

    private class SubscribeRequestTask extends AsyncTask<Request, Void, Request> {

        private String userId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Request doInBackground(final Request... params) {
            userId = Secure.getString(getActivity().getContentResolver(), Secure.ANDROID_ID);
            return requestService.subscribe(userId, registrationId, params[0]);
        }

        @Override
        protected void onPostExecute(final Request request) {
            super.onPostExecute(request);
            progressDialog.dismiss();

            final FragmentTransaction showSubscribeDialogTransaction = getFragmentManager().beginTransaction();
            final SubscribeDialogFragment subscribeDialog = SubscribeDialogFragment.newInstance(request);
            subscribeDialog.setSubscribeFinishHandler(AddRequestFragment.this);
            showSubscribeDialogTransaction.add(subscribeDialog, "Subscribe");
            showSubscribeDialogTransaction.commit();

            if (request.isSubscribed()) {
                try {
                    final RequestUnit requestUnit = new RequestUnit();
                    requestUnit.setUuid(userId);
                    requestUnit.setRequest(request);
                    HelperFactory.getHelper().getCategoryDao().save(request.getCategory());
                    HelperFactory.getHelper().getRequestDao().save(request);
                    HelperFactory.getHelper().getRequestUnitDao().save(requestUnit);
                } catch (SQLException e) {
                    Toast.makeText(getActivity(), "Request saving error", LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Subscription error", LENGTH_SHORT).show();
            }
        }
    }
}
