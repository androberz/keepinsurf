package org.dreworks.advparser.android.app.model.dao;

import org.dreworks.advparser.android.app.model.Request;

import java.sql.SQLException;

public interface RequestDao {

    Request getById(Integer id) throws SQLException;

    void save(Request request) throws SQLException;

    int refresh(Request request) throws SQLException;

    int delete(Request request) throws SQLException;
}
