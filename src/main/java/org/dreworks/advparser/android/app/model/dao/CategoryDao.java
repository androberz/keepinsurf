package org.dreworks.advparser.android.app.model.dao;

import org.dreworks.advparser.android.app.model.Category;

import java.sql.SQLException;

public interface CategoryDao {

    void save(Category category) throws SQLException;

    int delete(Category category) throws SQLException;
}
