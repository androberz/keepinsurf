package org.dreworks.advparser.android.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@DatabaseTable(tableName = "request_unit")
public class RequestUnit implements Serializable {

    public static final String UUID_COLUMN = "uuid";

    @DatabaseField(columnName = UUID_COLUMN, canBeNull = false, id = true)
    private String uuid;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 3)
    private Request request;
    @ForeignCollectionField(maxEagerForeignCollectionLevel = 3)
    private Collection<Result> results = new ArrayList<>();

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(final Request request) {
        this.request = request;
    }

    public Collection<Result> getResults() {
        return results;
    }

    public void setResults(final Collection<Result> results) {
        this.results = results;
    }

    public void addResults(final Collection<Result> resultsToAdd) {
        this.results.addAll(resultsToAdd);
    }

    public void removeResults(final Collection<Result> resultsToRemove) {
        this.results.removeAll(resultsToRemove);
    }
}
