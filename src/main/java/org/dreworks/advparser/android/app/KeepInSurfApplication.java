package org.dreworks.advparser.android.app;

import android.app.Application;
import org.dreworks.advparser.android.app.model.orm.HelperFactory;

public class KeepInSurfApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
