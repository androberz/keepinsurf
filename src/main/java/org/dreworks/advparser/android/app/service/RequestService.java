package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.Category;
import org.dreworks.advparser.android.app.model.OldAutoConfig;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.model.Result;

import java.util.List;

public interface RequestService {

    List<String> getCities();

    List<Category> getCategories();

    OldAutoConfig getOldAutoConfig();

    List<String> getModelsFor(String car);

    Request subscribe(String userId, String registrationId, Request request);

    boolean unsubscribe(String userId, Request request);

    List<Result> getResultsFor(String userId, Request request);

    List<Request> getRequestsFor(String userId);
}
