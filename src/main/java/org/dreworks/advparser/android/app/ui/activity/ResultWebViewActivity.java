package org.dreworks.advparser.android.app.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Result;
import org.dreworks.advparser.android.app.ui.web.AvitoWebViewClient;
import org.dreworks.advparser.android.app.utils.Settings;

import static org.dreworks.advparser.android.app.utils.AppUtils.showAdv;

public class ResultWebViewActivity extends FragmentActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview_result);

        final WebView webView = (WebView) findViewById(R.id.result_webview);

        webView.setWebViewClient(new AvitoWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);

        final Result result = (Result) getIntent().getSerializableExtra("result");
        webView.loadUrl(Settings.SITE_BASE_PATH + result.getLink());

        final RelativeLayout parentView = (RelativeLayout) findViewById(R.id.web_result_activity_layout);
        showAdv(this, parentView);
    }
}
