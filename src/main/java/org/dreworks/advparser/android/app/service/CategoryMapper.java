package org.dreworks.advparser.android.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryMapper {

    public static final Map<String, String> categoryMap = new HashMap<>();

    static {
        categoryMap.put("auto", "-- Транспорт --");
        categoryMap.put("oldAuto", "Автомобили с пробегом");
        categoryMap.put("newAuto", "Новые автомобили");
        categoryMap.put("hardVehicle", "Грузовики и спецтехника");
        categoryMap.put("waterVehicle", "Водный транспорт");
        categoryMap.put("parts", "Запчасти и аксессуары");
    }

    public List<String> getFriendlyCategories(final List<String> fromCategories) {
        final List<String> friendly = new ArrayList<>();
        for (final String fromCategory : fromCategories) {
            if (categoryMap.containsKey(fromCategory)) {
                friendly.add(categoryMap.get(fromCategory));
            } else {
                friendly.add(fromCategory);
            }
        }

        return friendly;
    }

    public String getDbName(final String friendlyName) {
        if (categoryMap.containsValue(friendlyName)) {
            for (final Map.Entry<String, String> entry : categoryMap.entrySet()) {
                if (entry.getValue().equals(friendlyName)) {
                    return entry.getKey();
                }
            }
        }

        return friendlyName;
    }
}
