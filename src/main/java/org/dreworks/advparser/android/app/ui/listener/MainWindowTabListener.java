package org.dreworks.advparser.android.app.ui.listener;

import android.app.ActionBar;
import android.support.v4.view.ViewPager;

public class MainWindowTabListener implements ActionBar.TabListener {

    private final ViewPager viewPager;

    public MainWindowTabListener(final ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    @Override
    public void onTabSelected(final ActionBar.Tab tab, final android.app.FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(final ActionBar.Tab tab, final android.app.FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(final ActionBar.Tab tab, final android.app.FragmentTransaction ft) {
    }
}
