package org.dreworks.advparser.android.app.ui.handler;

public interface SubscribeFinishHandler {

    void finishSubscription();
}
