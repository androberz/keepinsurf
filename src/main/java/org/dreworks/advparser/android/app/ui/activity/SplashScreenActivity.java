package org.dreworks.advparser.android.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import org.dreworks.advparser.android.R;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final int delayInMillis = getResources().getInteger(R.integer.splash_screen_timeout_in_millis);

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainActivityIntent = new Intent(getApplicationContext(), MainWindowActivity.class);
                startActivity(mainActivityIntent);
                finish();
            }
        }, delayInMillis);
    }
}
