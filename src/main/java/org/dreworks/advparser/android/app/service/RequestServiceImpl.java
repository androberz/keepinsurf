package org.dreworks.advparser.android.app.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.dreworks.advparser.android.app.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.apache.http.params.HttpConnectionParams.setConnectionTimeout;
import static org.apache.http.params.HttpConnectionParams.setSoTimeout;
import static org.dreworks.advparser.android.app.utils.Settings.*;

public class RequestServiceImpl implements RequestService {

    private final RequestJsonConverter requestJsonConverter = new RequestJsonConverterImpl();
    private final ResultJsonConverter resultJsonConverter = new ResultJsonConverterImpl();
    private final HttpClient httpClient;

    public RequestServiceImpl() {
        final HttpParams httpParams = new BasicHttpParams();
        setConnectionTimeout(httpParams, 10000);
        setSoTimeout(httpParams, 10000);
        httpClient = new DefaultHttpClient(httpParams);
    }

    @Override
    public List<String> getCities() {
        final List<String> cities = new ArrayList<>();
        final HttpGet request = new HttpGet(BASE_URI + CITIES_URI);

        try {
            final HttpResponse response = httpClient.execute(request);
            final HttpEntity httpEntity = response.getEntity();
            final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
            final BufferedReader reader = new BufferedReader(inputStreamReader);

            final StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final JSONArray jsonArray = new JSONArray(sb.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                cities.add(jsonArray.getString(i));
            }
        } catch (final JSONException | IOException ignored) {
        }

        return cities;
    }

    @Override
    public List<Category> getCategories() {
        final List<Category> categories = new ArrayList<>();
        final HttpGet request = new HttpGet(BASE_URI + CATEGORIES_URI);

        try {
            final HttpResponse response = httpClient.execute(request);
            final HttpEntity httpEntity = response.getEntity();
            final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
            final BufferedReader reader = new BufferedReader(inputStreamReader);

            final StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final JSONArray jsonArray = new JSONArray(sb.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final Category category = new Category();
                category.setParentNane(jsonObject.getString("parentName"));
                category.setName(jsonObject.getString("name"));
                categories.add(category);
            }
        } catch (final IOException | JSONException ignored) {
        }

        return categories;
    }

    @Override
    public OldAutoConfig getOldAutoConfig() {
        final AvitoOldAutoConfig oldAutoConfig = new AvitoOldAutoConfig();
        final HttpGet request = new HttpGet(BASE_URI + OLD_AUTO_CONFIG_URI);

        try {
            final HttpResponse response = httpClient.execute(request);
            final HttpEntity httpEntity = response.getEntity();
            final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
            final BufferedReader reader = new BufferedReader(inputStreamReader);

            final StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final JSONObject jsonObject = new JSONObject(sb.toString());

            final JSONArray carsArray = jsonObject.getJSONArray("cars");
            final List<String> cars = new ArrayList<>();
            for (int i = 0; i < carsArray.length(); i++) {
                cars.add(carsArray.getString(i));
            }
            oldAutoConfig.setCars(withEmptyFirstElement(cars));

            final JSONArray yearsArray = jsonObject.getJSONArray("years");
            final List<String> years = new ArrayList<>();
            for (int i = 0; i < yearsArray.length(); i++) {
                years.add(yearsArray.getString(i));
            }
            oldAutoConfig.setYears(withEmptyFirstElement(years));

            final JSONArray runsArray = jsonObject.getJSONArray("runs");
            final List<String> runs = new ArrayList<>();
            for (int i = 0; i < runsArray.length(); i++) {
                runs.add(runsArray.getString(i));
            }
            oldAutoConfig.setRuns(configRuns(runs));

            final JSONArray carBodiesArray = jsonObject.getJSONArray("carBodies");
            final List<String> carBodies = new ArrayList<>();
            for (int i = 0; i < carBodiesArray.length(); i++) {
                carBodies.add(carBodiesArray.getString(i));
            }
            oldAutoConfig.setCarBodies(withEmptyFirstElement(carBodies));

            final JSONArray gearsArray = jsonObject.getJSONArray("gears");
            final List<String> gears = new ArrayList<>();
            for (int i = 0; i < gearsArray.length(); i++) {
                gears.add(gearsArray.getString(i));
            }
            oldAutoConfig.setGears(withEmptyFirstElement(gears));

            final JSONArray engineCapacitiesArray = jsonObject.getJSONArray("engineCapacities");
            final List<Double> engineCapacities = new ArrayList<>();
            for (int i = 0; i < engineCapacitiesArray.length(); i++) {
                engineCapacities.add(engineCapacitiesArray.getDouble(i));
            }
            oldAutoConfig.setEngineCapacities(configEngineCapacities(engineCapacities));

            final JSONArray engineTypesArray = jsonObject.getJSONArray("engineTypes");
            final List<String> engineTypes = new ArrayList<>();
            for (int i = 0; i < engineTypesArray.length(); i++) {
                engineTypes.add(engineTypesArray.getString(i));
            }
            oldAutoConfig.setEngineTypes(withEmptyFirstElement(engineTypes));

            final JSONArray driveTypesArray = jsonObject.getJSONArray("driveTypes");
            final List<String> driveTypes = new ArrayList<>();
            for (int i = 0; i < driveTypesArray.length(); i++) {
                driveTypes.add(driveTypesArray.getString(i));
            }
            oldAutoConfig.setDriveTypes(withEmptyFirstElement(driveTypes));
        } catch (final JSONException | IOException ignored) {
        }

        return oldAutoConfig;
    }

    private List<String> configRuns(final List<String> origin) {
        final ArrayList<String> result = new ArrayList<>();

        for (Integer i = Integer.valueOf(origin.get(0)); i < Integer.valueOf(origin.get(1)); i = i + 5000) {
            result.add(i.toString());
        }
        for (Integer i = Integer.valueOf(origin.get(1)); i < Integer.valueOf(origin.get(2)); i = i + 10000) {
            result.add(i.toString());
        }
        for (Integer i = Integer.valueOf(origin.get(2)); i <= Integer.valueOf(origin.get(3)); i = i + 50000) {
            result.add(i.toString());
        }

        return withEmptyFirstElement(result);
    }

    private List<String> configEngineCapacities(final List<Double> origin) {
        final List<String> result = new ArrayList<>();

        for (Double i = origin.get(0); i < origin.get(1); i = i + 0.1) {
            result.add(String.format("%.1f", i));
        }
        for (Double i = origin.get(1); i <= origin.get(2); i = i + 0.5) {
            result.add(String.format("%.1f", i));
        }

        return withEmptyFirstElement(result);
    }

    private List<String> withEmptyFirstElement(final List<String> list) {
        final ArrayList<String> result = new ArrayList<>();
        result.add("");
        result.addAll(list);

        return result;
    }

    @Override
    public List<String> getModelsFor(final String car) {
        final List<String> models = new ArrayList<>();
        final HttpGet request = new HttpGet(BASE_URI + OLD_AUTO_CONFIG_URI + "/" + car.replace(" ", "%20").toLowerCase() + "/models");

        try {
            final HttpResponse response = httpClient.execute(request);
            final HttpEntity httpEntity = response.getEntity();
            final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
            final BufferedReader reader = new BufferedReader(inputStreamReader);

            final StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final JSONArray jsonArray = new JSONArray(sb.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                models.add(jsonArray.getString(i));
            }
        } catch (final IOException | JSONException ignored) {
        }

        return withEmptyFirstElement(models);
    }

    @Override
    public Request subscribe(final String userId, final String registrationId, final Request request) {
        final HttpPost httpPost = new HttpPost(BASE_URI + "/avito/auto/old/users/" + userId + "/requests");
        final JSONObject jsonObject = new JSONObject();
        try {
            final JSONObject categoryJson = new JSONObject();
            final JSONObject criteriaJson = new JSONObject();

            categoryJson.put("parentName", request.getCategory().getParentNane());
            categoryJson.put("name", request.getCategory().getName());

            criteriaJson.put("carBrand", request.getOldAutoCar());
            criteriaJson.put("model", request.getOldAutoCarModel());
            criteriaJson.put("startYear", request.getOldAutoStartYear());
            criteriaJson.put("endYear", request.getOldAutoEndYear());
            criteriaJson.put("startRun", request.getOldAutoStartRun());
            criteriaJson.put("endRun", request.getOldAutoEndRun());
            criteriaJson.put("carBody", request.getOldAutoCarBody());
            criteriaJson.put("gearType", request.getOldAutoGear());
            criteriaJson.put("startEngineCapacity", request.getOldAutoStartEngineCap());
            criteriaJson.put("endEngineCapacity", request.getOldAutoEndEngineCap());
            criteriaJson.put("engineType", request.getOldAutoEngineType());
            criteriaJson.put("driveType", request.getOldAutoDriveType());
            criteriaJson.put("startPrice", request.getOldAutoStartPrice());
            criteriaJson.put("endPrice", request.getOldAutoEndPrice());
            criteriaJson.put("hasPhoto", request.getOldAutoHasPhoto());

            jsonObject.put("city", request.getCity());
            jsonObject.put("category", categoryJson);
            jsonObject.put("criteria", criteriaJson);
            jsonObject.put("registrationId", registrationId);
        } catch (final JSONException ignored) {
        }

        try {
            httpPost.setEntity(new StringEntity(jsonObject.toString(), "UTF-8"));
            httpPost.setHeader("Content-type", "application/json");
            final HttpResponse httpResponse = httpClient.execute(httpPost);
            if (httpResponse != null) {
                if (httpResponse.getStatusLine().getStatusCode() == 201) {
                    request.setSubscribed(true);

                    final HttpEntity httpEntity = httpResponse.getEntity();
                    final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
                    final BufferedReader reader = new BufferedReader(inputStreamReader);

                    final StringBuilder sb = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    final JSONObject retrieved = new JSONObject(sb.toString());
                    final int retrievedUid = retrieved.getInt("uid");
                    request.setId(retrievedUid);
                } else {
                    request.setSubscribed(false);
                }
            }
        } catch (final IOException | JSONException ignored) {
        }

        return request;
    }

    @Override
    public boolean unsubscribe(final String userId, final Request request) {
        boolean result = true;
        final HttpDelete httpDelete = new HttpDelete(BASE_URI + "/avito/auto/old/users/" + userId + "/requests/" + request.getId());
        httpDelete.setHeader("Content-type", "application/json");
        try {
            final HttpResponse httpResponse = httpClient.execute(httpDelete);
            if (null != httpResponse) {
                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                    request.setSubscribed(false);
                } else {
                    result = false;
                }
            }
        } catch (final IOException e) {
            result = false;
        }

        return result;
    }

    @Override
    public List<Result> getResultsFor(final String userId, final Request request) {
        final List<Result> results = new ArrayList<>();

        final HttpGet httpGet = new HttpGet(BASE_URI + "/avito/auto/old/users/" + userId + "/requests/" + request.getId() + "/results");
        httpGet.setHeader("Content-type", "application/json");

        try {
            final HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HTTP_OK) {
                final HttpEntity httpEntity = response.getEntity();
                final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
                final BufferedReader reader = new BufferedReader(inputStreamReader);

                final StringBuilder sb = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                final JSONArray jsonArray = new JSONArray(sb.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    final JSONObject jsonResult = jsonArray.getJSONObject(i);
                    final Result result = resultJsonConverter.fromJson(jsonResult);
                    results.add(result);
                }
            }
        } catch (final IOException | JSONException ignored) {
        }

        return results;
    }

    @Override
    public List<Request> getRequestsFor(final String userId) {
        final ArrayList<Request> foundRequests = new ArrayList<>();
        final HttpGet httpGet = new HttpGet(BASE_URI + "/avito/auto/old/users/" + userId + "/requests");

        try {
            final HttpResponse response = httpClient.execute(httpGet);
            final HttpEntity httpEntity = response.getEntity();
            final InputStreamReader inputStreamReader = new InputStreamReader(httpEntity.getContent());
            final BufferedReader reader = new BufferedReader(inputStreamReader);

            final StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final JSONArray jsonArray = new JSONArray(sb.toString());

            for (int i = 0; i < jsonArray.length(); i++) {
                final JSONObject jsonRequest = jsonArray.getJSONObject(i);
                final Request request = requestJsonConverter.fromJson(jsonRequest);
                foundRequests.add(request);
            }
        } catch (final IOException | JSONException ignored) {
            return null;
        }

        return foundRequests;
    }
}
