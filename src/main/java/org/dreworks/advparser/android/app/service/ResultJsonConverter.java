package org.dreworks.advparser.android.app.service;

import org.dreworks.advparser.android.app.model.Result;
import org.json.JSONException;
import org.json.JSONObject;

public interface ResultJsonConverter {

    Result fromJson(JSONObject jsonResult) throws JSONException;
}
