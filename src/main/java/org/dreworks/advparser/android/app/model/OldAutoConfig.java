package org.dreworks.advparser.android.app.model;

import java.util.List;

public interface OldAutoConfig {

    List<String> getCars();

    List<String> getYears();

    List<String> getRuns();

    List<String> getCarBodies();

    List<String> getGears();

    List<String> getEngineCapacities();

    List<String> getEngineTypes();

    List<String> getDriveTypes();
}
