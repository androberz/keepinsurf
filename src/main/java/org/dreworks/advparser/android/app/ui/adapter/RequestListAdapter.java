package org.dreworks.advparser.android.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import org.dreworks.advparser.android.R;
import org.dreworks.advparser.android.app.model.Request;
import org.dreworks.advparser.android.app.ui.handler.RequestListEntryViewResultsHandler;

public class RequestListAdapter extends ArrayAdapter<Request> {

    private final int layoutResource;

    public RequestListAdapter(final Context context, final int layoutResource) {
        super(context, 0);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View view = getWorkingView(convertView);
        final ViewHolder viewHolder = getViewHolder(view);
        final Request entry = getItem(position);

        viewHolder.city.setText(entry.getCity());
        viewHolder.category.setText(entry.getCategory().toString());
        viewHolder.car.setText(entry.getOldAutoCar());
        viewHolder.carModel.setText(entry.getOldAutoCarModel());
        viewHolder.viewUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final RequestListEntryViewResultsHandler viewResultsHandler = (RequestListEntryViewResultsHandler) convertView.getContext();
                viewResultsHandler.viewResults(entry, true);
            }
        });
        viewHolder.viewResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final RequestListEntryViewResultsHandler viewResultsHandler = (RequestListEntryViewResultsHandler) convertView.getContext();
                viewResultsHandler.viewResults(entry, false);
            }
        });

        return view;
    }

    private View getWorkingView(final View convertView) {
        final View workingView;

        if (null == convertView) {
            final LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            workingView = layoutInflater.inflate(layoutResource, null);
        } else {
            workingView = convertView;
        }

        return workingView;
    }

    private ViewHolder getViewHolder(final View workingView) {
        final Object tag = workingView.getTag();
        final ViewHolder viewHolder;

        if (null == tag || !(tag instanceof ViewHolder)) {
            viewHolder = new ViewHolder();
            viewHolder.city = (TextView) workingView.findViewById(R.id.request_item_city);
            viewHolder.category = (TextView) workingView.findViewById(R.id.request_item_category);
            viewHolder.car = (TextView) workingView.findViewById(R.id.request_item_car);
            viewHolder.carModel = (TextView) workingView.findViewById(R.id.request_item_car_model);
            viewHolder.viewUpdates = (Button) workingView.findViewById(R.id.view_updates_button);
            viewHolder.viewResults = (Button) workingView.findViewById(R.id.view_results_button);

            workingView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) tag;
        }

        return viewHolder;
    }

    private static class ViewHolder {
        public TextView city;
        public TextView category;
        public TextView car;
        public TextView carModel;
        public Button viewUpdates;
        public Button viewResults;
    }
}
