package org.dreworks.advparser.android.app.ui.handler;

import org.dreworks.advparser.android.app.model.Request;

public interface RequestListEntryViewResultsHandler {

    void viewResults(Request request, boolean isUpdate);
}
