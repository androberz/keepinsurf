package org.dreworks.advparser.android.app.model.dao;

import org.dreworks.advparser.android.app.model.RequestUnit;

import java.sql.SQLException;

public interface RequestUnitDao {

    RequestUnit getById(String id) throws SQLException;

    void save(RequestUnit requestUnit) throws SQLException;

    int delete(RequestUnit requestUnit) throws SQLException;
}
