package org.dreworks.advparser.android.app.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "result")
public class Result implements Serializable {

    public static final String REQUEST_UNIT_ID = "request_unit_id";
    public static final String SITE_ID = "site_id";
    public static final String PHOTO_LINK = "photo_link";
    public static final String DATE = "date";
    public static final String DESCRIPTION = "description";
    public static final String LINK = "link";

    @DatabaseField(foreign = true, columnName = REQUEST_UNIT_ID)
    private RequestUnit requestUnit;
    @DatabaseField(columnName = SITE_ID, id = true)
    private String siteId;
    @DatabaseField(columnName = PHOTO_LINK)
    private String photoLink;
    @DatabaseField(columnName = DATE)
    private String date;
    @DatabaseField(columnName = DESCRIPTION)
    private String description;
    @DatabaseField(columnName = LINK)
    private String link;

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(final String photoLink) {
        this.photoLink = photoLink;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(final String siteId) {
        this.siteId = siteId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public RequestUnit getRequestUnit() {
        return requestUnit;
    }

    public void setRequestUnit(final RequestUnit requestUnit) {
        this.requestUnit = requestUnit;
    }

    @Override
    public boolean equals(final Object o) {
        return o instanceof Result && ((Result) o).getSiteId().equals(siteId);
    }
}
