package org.dreworks.advparser.android.app.model.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import org.dreworks.advparser.android.app.model.Category;

import java.sql.SQLException;

public class CategoryDaoImpl extends BaseDaoImpl<Category, Integer> implements CategoryDao {

    public CategoryDaoImpl(final ConnectionSource connectionSource, final Class<Category> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public void save(final Category category) throws SQLException {
        create(category);
    }

    @Override
    public int delete(final Category category) throws SQLException {
        return super.delete(category);
    }
}
